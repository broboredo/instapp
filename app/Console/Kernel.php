<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();

        $username = 'wallacemoreira@sapo.pt';
        $password = 'porracaralhO!';

        $schedule->command('instapp:like:discover', [
            'username' => $username,
            'password' => $password
        ])->cron('*/30 * * * *');

        $schedule->command('instapp:follow:discover', [
            'username' => $username,
            'password' => $password
        ])->cron('0 */2 * * *');

        //$schedule->command('logs:remove')->weekly();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
