<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;

class RemoveLogFiles extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'logs:remove';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Remove all log files';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $fileSystem = new Filesystem();
        $files = $fileSystem->allFiles(storage_path('logs'));

        foreach ($files as $file) {
            $fileSystem->delete($file->getPathname());
        }
    }
}
