<?php

namespace App\Console\Commands\Like;

use Carbon\Carbon;
use Illuminate\Console\Command;
use InstagramAPI\Instagram;

class Discover extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'instapp:like:discover 
                            {username : Your Instagram username} 
                            {password : Your Instagram Account password}
                            {--follow : Follow the user from media liked}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Like medias from discover section';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $maxLikes = 200;
        $username = $this->argument('username');
        $password = $this->argument('password');
        $follow = $this->option('follow', false);

        $proxy = 'tcp://181.112.52.26:53281';

        $ig = new Instagram((env('APP_ENV') == 'local'), false);
        $ig->login($username, $password);
        //$ig->setProxy(['proxy' => $proxy]);

        $module = 'feed_contextual_newsfeed_multi_media_liked';

        $start = Carbon::now();

        $maxId = null;
        $likesCounter = 0;
        do {
            $response = $ig->discover->getExploreFeed();
            $search = $response->getItems();

            $maxId = $response->getNextMaxId();

            foreach ($search as $s) {
                $ig->media->like($s->getMedia()->getId(), $module);
                $likesCounter++;
                $this->info("Liked a total of $likesCounter medias until now");

                if($follow !== false) {
                    sleep(2);
                    $ig->people->follow($s->getMedia()->getUser()->getPk());
                }

                sleep(4);
            }

        } while ($maxId !== null && $likesCounter < $maxLikes);

        $finish = Carbon::now();

        $this->alert("Liked a total of $likesCounter medias at $finish");
        $this->info($start->diffForHumans($finish));
    }
}
