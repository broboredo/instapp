<?php

namespace App\Console\Commands\Like;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use InstagramAPI\Exception\BadRequestException;
use InstagramAPI\Instagram;
use InstagramAPI\Signatures;

class Hashtag extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'instapp:like:hashtag 
                            {username : Your Instagram username} 
                            {password : Your Instagram Account password}
                            {hashtag : The Hashtag that you want like medias}
                            {--follow : Follow the user from media liked}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Like medias from discover section';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $maxLikes = 200;
        $username = $this->argument('username');
        $password = $this->argument('password');
        $hashTag = $this->argument('hashtag');
        $follow = $this->option('follow', false);

        $proxy = 'tcp://181.112.52.26:53281';

        $ig = new Instagram((env('APP_ENV') == 'local'), false);
        $ig->login($username, $password);
        //$ig->setProxy(['proxy' => $proxy]);


        $module = 'feed_contextual_hashtag';
        $extraData = ['hashtag' => $hashTag];

        $maxId = null;
        $likesCounter = 0;
        do {
            $response = $ig->hashtag->getFeed($hashTag, Signatures::generateUUID());
            $search = $response->getItems();

            $start = Carbon::now();

            $maxId = $response->getNextMaxId();
            foreach ($search as $s) {
                try {
                    $ig->media->like($s->getId(), $module, $extraData);
                    $likesCounter++;
                    $this->info("Liked a total of $likesCounter medias until now");

                    if ($follow !== false) {
                        sleep(2);
                        $ig->people->follow($s->getUser()->getPk());
                    }

                    sleep(6);
                } catch (BadRequestException $e) {
                    Log::error($e->getMessage(), [
                        'code' => $e->getCode(),
                        'class' => __CLASS__,
                        'media_id' => $s->getId()
                    ]);

                    if($e->getCode() == 429) {
                        $this->info("Sleeping for 2 minutes");
                        sleep(120);
                    }
                }
            }

        } while ($maxId !== null && $likesCounter < $maxLikes);

        $finish = Carbon::now();

        $this->alert("Liked a total of $likesCounter medias at $finish");
        $this->info($start->diffForHumans($finish));
    }
}
