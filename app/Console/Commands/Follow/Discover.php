<?php

namespace App\Console\Commands\Follow;

use Illuminate\Console\Command;
use InstagramAPI\Instagram;
use InstagramAPI\Signatures;

class Discover extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'instapp:follow:discover 
                            {username : Your Instagram username} 
                            {password : Your Instagram Account password}
                            {--like}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Follow user from discover media posted';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $maxFollows = 200;
        $username = $this->argument('username');
        $password = $this->argument('password');
        $like = $this->option('like', false);
        $proxy = 'tcp://181.112.52.26:53281';

        $ig = new Instagram((env('APP_ENV') == 'local'), false);
        $ig->login($username, $password);
        //$ig->setProxy(['proxy' => $proxy]);

        $module = 'feed_contextual_newsfeed_multi_media_liked';

        $maxId = null;
        $followCounter = 0;
        do {
            $response = $ig->discover->getExploreFeed();
            $search = $response->getItems();

            $maxId = $response->getNextMaxId();

            foreach ($search as $s) {
                $alreadyFollowed = $this->alreadyFollowed(
                    $s->getMedia()->getUser()->getPk(),
                    $ig->people->getSelfFollowing(Signatures::generateUUID())->getUsers()
                );

                if ($alreadyFollowed) {
                    continue;
                }

                $ig->people->follow($s->getMedia()->getUser()->getPk());
                $followCounter++;
                $this->info("Following a total of $followCounter new users until now");

                if($like !== false) {
                    sleep(2);
                    $ig->media->like($s->getMedia()->getId(), $module);
                }

                sleep(4);
            }

        } while ($maxId !== null && $followCounter < $maxFollows);

        $this->alert("Following a total of $followCounter new users.");
    }

    private function alreadyFollowed($userPk, $followed)
    {
        $neededObject = array_filter(
            $followed,
            function ($e) use ($userPk) {
                return $e->getPk() == $userPk;
            }
        );

        return empty($neededObject) ? false : true;
    }
}
