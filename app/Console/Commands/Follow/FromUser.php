<?php

namespace App\Console\Commands\Follow;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use InstagramAPI\Instagram;
use InstagramAPI\Signatures;

class FromUser extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'instapp:follow:from_user 
                            {username : Your Instagram username} 
                            {password : Your Instagram Account password}
                            {fromUsername : Username that you want follow the users that user are following}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $username = $this->argument('username');
        $password = $this->argument('password');
        $fromUsername = $this->argument('fromUsername');
        $proxy = 'tcp://160.19.246.236:35442';

        $ig = new Instagram((env('APP_ENV') == 'local'), false);
        $ig->login($username, $password);
        $ig->setProxy(['proxy' => $proxy]);

        $maxId = null;
        do {
            if(is_null($userId = $ig->people->getUserIdForName($fromUsername)))
                throw new \Exception('User not found', '500');

            $response = $ig->people->getFollowing($userId, Signatures::generateUUID());
            $users = $response->getUsers();

            $userThatIFollow = $ig->people->getSelfFollowing(Signatures::generateUUID())->getUsers();

            $maxId = $response->getNextMaxId();
            $sleepByEach = 0;

            $this->info('enter in for');
            foreach ($users as $user) {
                try {
                    $userPk = $user->getPk();

                    $alreadyFollowed = $this->alreadyFollowed(
                        $userPk,
                        $userThatIFollow
                    );

                    if ($alreadyFollowed) {
                        $this->info('continue');
                        continue;
                    }

                    if ($sleepByEach > 0 && $sleepByEach % 150 == 0) {
                        $this->info('Sleeping for 5 minutes');
                        sleep(60*5);
                    }

                    $ig->people->follow($userPk);
                    $sleepByEach++;
                    $this->info('Following user ' . $user->getUsername() . ' now');

                    sleep(8);
                } catch (\Exception $e) {
                    Log::error($e->getMessage(), [
                        'code' => $e->getCode()
                    ]);

                    if($e->getCode() == 429) {
                        $this->alert('Sleeping for 5 minutes');
                        sleep(60*5);
                    }
                }
            }

        } while ($maxId !== null);

        $this->alert("Finished");
    }

    private function alreadyFollowed($userPk, $followed)
    {
        $neededObject = array_filter(
            $followed,
            function ($e) use ($userPk) {
                return $e->getPk() == $userPk;
            }
        );

        return empty($neededObject) ? false : true;
    }
}
