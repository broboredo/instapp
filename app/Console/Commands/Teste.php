<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use InstagramAPI\Exception\FeedbackRequiredException;
use InstagramAPI\Instagram;
use InstagramAPI\Signatures;

class Teste extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'teste:insta';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $ig = new Instagram(true, false);
        //$ig->login('wallacemoreira@sapo.pt', 'Qwaszx12!$#');
        $ig->login('roboredo.bruno@gmail.com', 'Qwaszx12!');
        //https://free-proxy-list.net/
        //$ig->setProxy(['proxy' => 'tcp://181.112.52.26:53281']);

        //$userId = $ig->people->getUserIdForName('broboredo');
        //get followers from user $followers = $ig->people->getFollowers($userId, Signatures::generateUUID());

//        $recipients =
//            [
//                'users' => [
//                    $userId
//                ]// must be an [array] of valid UserPK IDs
//            ];
        //$ig->direct->sendText($recipients, 'hey hellow. text to me!!!!');

        //story photo $ig->story->uploadPhoto('/Users/broboredo/Downloads/download.jpeg', ['caption' => '#followme', 'link' => 'https://g.foolcdn.com/image/?url=https%3A%2F%2Fg.foolcdn.com%2Feditorial%2Fimages%2F502052%2Fbitcoin4.jpg&w=700&op=resize']);
        //timeline photo $ig->timeline->uploadPhoto('/Users/broboredo/Downloads/bitocin.jpg', ['caption' => '#bitcoin']);

        /*
         *
         * TODO: curtindo por hashtag e seguindo dono da foto curtida
         *
        $hashTag = "followtofollow";
        $rankToken = Signatures::generateUUID();


        $search = $ig->hashtag->getFeed($hashTag, $rankToken);
        $module = 'feed_contextual_hashtag';
        $extraData = ['hashtag' => $hashTag];
        $count = 0;

        while ($search->isMoreAvailable()) {
            foreach ($search->getItems() as $s) {
                $count++;
                try {
                    $alreadyFollowed = $this->alreadyFollowed(
                        $s->getUser()->getPk(),
                        $ig->people->getSelfFollowing(Signatures::generateUUID())->getUsers()
                    );

                    if ($alreadyFollowed) {
                        $this->info("ja segue \n");
                        continue;
                    }


                    //$this->info("curtindo");
                    //$ig->media->like($s->getId(), $module, $extraData);

                    $this->info("seguindo...\n");
                    $ig->people->follow($s->getUser()->getPk());

//                    $seconds = rand(3, 9);
//                    $this->info("esperando $seconds segundos...\n\n");
//                    sleep($seconds);
                } catch (FeedbackRequiredException $e) {
                    Log::error($e->getMessage(), [
                        'code' => $e->getCode(),
                        'response_message' => $e->getResponse()->getMessage()
                    ]);

                    if ($e->getResponse()->getSpam()) {
                        $this->alert('waiting for 1/2 hour');
                        $module = 'feed_contextual_post';
                        sleep(60 * 30);
                    }
                }
            }
        }

        $this->alert('FIM! ' . $count);
        */




        /*

                *
                *
                *
                * TODO: curtir por hashtag
         *
         */





                $maxId = null;
                $count = 0;
                do {
                   $hashTag = "vascodagama";
                   $rankToken = Signatures::generateUUID();

                   $search = $ig->hashtag->getFeed($hashTag, $rankToken, $maxId)->getItems();
                    $response = $ig->discover->getExploreFeed();
                   $search = $response->getItems();



                   $module = 'feed_contextual_newsfeed_multi_media_liked';

                    $maxId = $response->getNextMaxId();

                   foreach ($search as $s) {
                       //$ig->media->like($s->getId(), $module);
                       $ig->media->like($s->getMedia()->getId(), $module);
                       $count++;
                       $this->info('liked count:'.$count);
                       //$seconds = rand(3, 12);
                       sleep(4);
                   }

                } while ($maxId !== null && $count < 200);

                $this->alert("count:: $count");





               /*
                *
                * TODO: curtir todas fotos da timeline de um usuario
               $userId = $ig->people->getUserIdForName('rafaelmagalhaes');
               $images  = $ig->timeline->getUserFeed($userId)->getItems();


               $module = "media_view_profile";
               foreach ($images as $s) {
                   $ig->media->like($s->getId(), $module);
               }
                */
    }

    private function alreadyFollowed($userPk, $followed)
    {
        $neededObject = array_filter(
            $followed,
            function ($e) use ($userPk) {
                return $e->getPk() == $userPk;
            }
        );

        return empty($neededObject) ? false : true;
    }
}
